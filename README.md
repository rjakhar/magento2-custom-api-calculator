# magento2-custom-api-calculator

This module allowed act as a calculator for API POST method.

# Installation

1.Copy the files to app/code/ directory.

2.Execute the following commands

php bin/magento setup:upgrade

php bin/magento setup:static-content:deploy

php bin/magento cache:flush

php bin/magento cache:clean

# Instruction

API REST URL : http://YOURSITEURL/index.php/rest/V1/rce/calculator
POST Paramaters

{
"left" : 2.6,

"right" : 5.5,

"operator" : "add",

"precision" : 3

}

1.By default the precision is set to 2 deciaml, it is not necessory to send in paramaters.its an optional

2.Available methods are add, subtract, multiply, divide, power

3.Use the following commands

curl -d '{"left":2, "right" : 3, "operator" : "power", "precision" : 1}' -H 'Content-Type: application/json' http://YOURSITEURL/index.php/rest/V1/rce/calculator