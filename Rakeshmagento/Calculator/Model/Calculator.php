<?php
namespace Rakeshmagento\Calculator\Model;

use Rakeshmagento\Calculator\Api\CalculatorInterface;
use Magento\Framework\Exception\State\InvalidTransitionException;

class Calculator implements CalculatorInterface
{
    private $allowedOperations = [];

    public function __construct()
    {
        $this->allowedOperations = ['add','subtract','multiply','divide','power'];
    }

    /**
     * Return the desired result.
     *
     * @api
     * @param float $left Left hand operand.
     * @param float $right Right hand operand.
     * @param string $operator operation to be performed.
     * @param int $right precision.
     * @return float The result.
     */
    public function execute($left, $right, $operator, $precision = 2)
    {
        try {
            $this->validateRequestData($left, $right, $operator);
            $result = $this->performAction($left, $right, $operator);
            return number_format($result, $precision);
        } catch (\Exception $e) {
            throw new InvalidTransitionException(__('An error occurred and we could not process the request!'));
        }
    }

    /**
     * @param $left
     * @param $right
     * @param $operator
     */
    private function validateRequestData($left, $right, $operator)
    {
        if (!isset($left) || !isset($right) || !in_array($operator, $this->allowedOperations)) {
            throw new \InvalidArgumentException(__('all input values should be valid!'));
        }
    }

    /**
     * @param $var1
     * @param $var2
     * @param $oper
     */
    private function performAction($var1, $var2, $oper)
    {
        $res = null;
        switch ($oper) {
            case 'add':
                $res = $var1 + $var2;
                break;
            case 'subtract':
                $res = $var1 - $var2;
                break;
            case 'multiply':
                $res = $var1 * $var2;
                break;
            case 'divide':
                $res = $var1 / $var2;
                break;
            case 'power':
                $res = pow($var1, $var2);
                break;
        }
        return $res;
    }
}
