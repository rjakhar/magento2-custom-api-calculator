<?php
namespace Rakeshmagento\Calculator\Api;

interface CalculatorInterface
{
    /**
     * Return the desired result.
     *
     * @api
     * @param float $left Left hand operand.
     * @param float $right Right hand operand.
     * @param string $operator operation to be performed.
     * @param int $right precision.
     * @return float The result.
     */
    public function execute($left, $right, $operator, $precision = 2);
}
